const nodemailer = require('nodemailer');
require('dotenv').config();

const transport = nodemailer.createTransport({
    host: process.env.HOST,
    port: process.env.PORT,
    secure: process.env.SECURE, // true somente para as portas 465, as demais deverão ser false.
    auth: {
        user: process.env.USER,
        pass: process.env.PASS,
    }
});

transport.sendMail({
    from: `Envido com nodemailer ${process.env.USER}`,
    to: process.env.TO,
    subject: 'Enviando e-mail com o nodemailer',
    html: '<h1>Olá Dev!</h1> <p>Este e-mail foi enviado usando o nodemailer.</p>',
    text: 'Olá Dev, este e-mail foi enviado usando o nodemailer'
}).then(
    (resp) => console.log('Email enviado.', resp)
) .catch(
    (erro) => console.log('Erro ao enviar', erro)
)